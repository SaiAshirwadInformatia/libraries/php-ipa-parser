<?php

namespace SaiAshirwadInformatia\Parsers\Services;

use Exception;
use SaiAshirwadInformatia\Parsers\Models\IPA;
use ZipArchive;

class IPAParser
{

    /**
     * @param $file
     */
    public static function load($file)
    {
        if (!file_exists($file)) {
            throw new Exception("File not found: " . $file);
        }
        $zip = new ZipArchive;
        if ($zip->open($file) === true) {
            $directory = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid();
            if (!\file_exists($directory)) {
                @mkdir($directory);
            }
            $zip->extractTo($directory);
            echo "Extracted to: $directory " . PHP_EOL;
            $zip->close();
            $payloadDir = $directory . DIRECTORY_SEPARATOR . 'Payload' . DIRECTORY_SEPARATOR;
            $files      = array_slice(scandir($payloadDir), 2);
            $appDir     = $payloadDir . $files[0] . DIRECTORY_SEPARATOR;

            $ipa = new IPA($appDir);

            self::rrmdir($appDir);
            return $ipa;
        } else {
            throw new Exception("Invalid IPA File: " . $file);
        }
    }

    /**
     * @param $src
     */
    private static function rrmdir($src)
    {
        $dir = opendir($src);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = $src . '/' . $file;
                if (is_dir($full)) {
                    self::rrmdir($full);
                } else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }
}
