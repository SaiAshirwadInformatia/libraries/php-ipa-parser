<?php

namespace SaiAshirwadInformatia\Parsers\Models;

use CFPropertyList\CFPropertyList;
use SaiAshirwadInformatia\Parsers\Models\Orientation;

class Info
{

    /**
     * @var mixed
     */
    protected $name;

    /**
     * @var mixed
     */
    protected $versionName;

    /**
     * @var mixed
     */
    protected $versionCode;

    /**
     * @var mixed
     */
    protected $identifier;

    /**
     * @var mixed
     */
    protected $minOSVersion;

    /**
     * @var mixed
     */
    protected $build;

    /**
     * @var mixed
     */
    protected $permissions;

    /**
     * @var array
     */
    protected $orientations;

    /**
     * @param CFPropertyList $infoPlist
     */
    public function __construct(CFPropertyList $infoPlist)
    {
        $info = $infoPlist->getValue()->getValue();
        $keys = [
            'CFBundleName'               => 'name',
            'CFBundleVersion'            => 'versionCode',
            'CFBundleShortVersionString' => 'versionName',
            'CFBundleIdentifier'         => 'identifier',
            'MinimumOSVersion'           => 'minOSVersion',
        ];
        foreach ($keys as $key => $prop) {
            if (isset($info[$key])) {
                $this->$prop = $info[$key]->getValue();
                unset($info[$key]);
            }
        }
        $this->build = new BuildInfo($info);

        foreach ($info as $property => $value) {
            if (strpos($property, 'NS') === 0 && strpos($property, 'Description') !== false) {
                $field               = str_replace('Description', '', substr($property, 2));
                $this->permissions[] = new Permission($field, $value->getValue());
            }
        }

        $this->orientations         = new \stdClass;
        $this->orientations->ipad   = new Orientation($info['UISupportedInterfaceOrientations~ipad'] ?? []);
        $this->orientations->iphone = new Orientation($info['UISupportedInterfaceOrientations'] ?? []);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key ?? null;
    }
}
